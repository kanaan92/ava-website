import React from "react";
import Header from "./components/Header";
import ArticleList from "./components/ArticleList";

const ArticlesPage = () => {
  return (
    <div>
      <Header />
      <ArticleList />
    </div>
  );
};

export default ArticlesPage;
