import testImage from "../assets/images/aboutus/dubai.webp";
import kasra from "../assets/images/aboutus/kasra.webp";
import cover from "../assets/images/articles/1.webp";
import buble from "../assets/images/articles/buble.webp";
import intro from "../assets/images/articles/intro.webp";
export const data = {
	headerTitle: "Read Our Latest Artilces and News",
	articles: [
		{
			createDate: "18/08/2023",
			slug: "Eergrande_Bankruptcy",
			pageTitle: "Eergrande Bankruptcy",
			mainImage: cover,
			title:
				"Implications of Evergrande's Bankruptcy on Dubai's Real Estate Market",
			minRead: "10",
			author: { name: "Kasra Malakouti", image: kasra },
			keywords:
				"dubai real estate, buildings economy, Evergrande's bankruptcy, Chinese real estate giant,Chinese real estate, China's Economy, Evergrande Fallout, Economic growth, Impact on China's Economy, effect of Evergrande's bankruptcy on Dubai Real Estate Market, global economic, Dubai Economy, Kasra Malakouti, AVA Real Estate",
			references: [
				"https://docs.google.com/document/d/1g1TMgUgbdLzxdZKFCVwV_TUSUZPZmkziTa_aCMWDWVY/edit",
				"https://www.figma.com/file/sMlPEBuOMgKEWujYchQAqT/AVA-realestate?node-id=282%3A1981&mode=dev",
			],
			sections: [
				{
					heading: "Introduction:",
					text: [
						"The recent announcement of Evergrande's bankruptcy has sent shockwaves through the global financial landscape. The Chinese real estate giant's financial troubles were a long-standing issue, finally culminating in its official declaration of bankruptcy in 2023. This development is particularly concerning given the significant role Evergrande played in China's real estate market, which accounts for a substantial portion of the country's economy. While China anticipated and attempted to mitigate the fallout, the repercussions of Evergrande's bankruptcy extend beyond its borders. This article delves into the potential impact of Evergrande's bankruptcy on Dubai's real estate market, considering its global economic significance and Dubai's position as a hub for international investments.",
					],
					images: [intro],
				},
				{
					heading: "The Evergrande Fallout and Its Impact on China's Economy:",
					text: [
						"Evergrande's bankruptcy is no surprise, given its prolonged financial instability dating back to 2021, characterized by liabilities exceeding $300 billion. Despite efforts by the Chinese government to delay the inevitable, the company's financial woes reached a point of no return in 2023, leading to its formal bankruptcy filing. Notably, Evergrande had a direct influence on approximately 40% of China's real estate and development market, making it a linchpin of the nation's economic growth.",
						"China, as the world's second-largest economy, holds a significant position in the global economic landscape, with a share of nearly 19% of the world economy. The anticipation of a recession in China post-Evergrande's bankruptcy casts a shadow over global economic stability. The repercussions may extend beyond China's borders and lead to a ripple effect on economies around the world.",
					],
					images: [buble],
				},
				{
					heading: "The Dubai Real Estate Market and Potential Benefits:",
					text: [
						"As investors seek safe havens amid financial turbulence, Dubai's real estate market emerges as a viable option. Dubai's reputation as a global business hub and its investor-friendly policies have attracted substantial foreign investment over the years. Evergrande's bankruptcy could further fuel this trend, as Chinese real estate investors may look to diversify their portfolios beyond their home country.",
						"China's real estate investors, historically ranking 15th in terms of nationality ownership in 2017, have displayed a remarkable appetite for international real estate assets. By 2024, it is projected that they could climb to the 5th rank in this ranking, driven by the need to safeguard their investments. Comparing data, it's evident that Chinese individuals increased their real estate acquisitions by an astonishing 363% when compared to the same day of the previous year.",
						"Dubai's real estate market, already attractive due to its strategic location, advanced infrastructure, and tax incentives, could witness an influx of Chinese capital seeking stable and lucrative investments. This capital inflow would likely drive up property prices and create heightened demand within the Dubai real estate market.",
					],
					images: [cover],
				},
				{
					heading: "Conclusion:",
					text: [
						"Evergrande's bankruptcy has cast a spotlight on the vulnerabilities of the global economic landscape. China's real estate market, a significant contributor to its economy, is expected to experience turbulence with potential cascading effects. The aftermath of this crisis could lead to Chinese investors diversifying their holdings into international markets, particularly in locations like Dubai, which offer stability and growth potential. Dubai's real estate market, positioned to accommodate such investment inflows, stands to benefit from this trend. As the world watches the fallout of Evergrande's bankruptcy, the dynamics of global real estate investments could see a notable shift, with Dubai poised to emerge as a prominent beneficiary.",
					],
					images: [],
				},
			],
		},
	],
};
